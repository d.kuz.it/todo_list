import React, {useEffect, useState} from 'react';
import './App.css';
import {Todolist} from "./Todolist";
import {v1} from "uuid";


export type FilterValuesType = 'ALL' | 'ACTIVE' | 'COMPLETED'
function App() {

    const  [task,setTask ] = useState( [
        {id: v1(), title: "CSS", isDone: false},
        {id: v1(), title: "React", isDone: true},
        {id: v1(), title: "JS", isDone: true},
        {id: v1(), title: "JS", isDone: false}
    ])
    const  [filter, setFilter] = useState<FilterValuesType>("ALL")


   const changeInputStatus = (taskId: string, isDone: boolean)=> {
        let tasks = task.find(t=> t.id === taskId)
            if (tasks){
            tasks.isDone = isDone
        }
    let copy = [ ...task ]
    setTask(copy)
}

  const changeFilter = (value: FilterValuesType)=>{
        setFilter(value)
  }
  const removeTask = (id: string) => {

        let resultTask = task.filter((task)=>
              task.id !== id)
      setTask(resultTask)
  }

  const addTask = (text : string)=> {
        if(!text){
            window.alert('cannot send empty string')
        } else {
            let newTask = {id: v1(), title: text, isDone: false}
            let newArrayTask = [newTask, ...task];
            setTask(newArrayTask)
        }

  }
  let  filterTask = task;
        if(filter === 'ACTIVE') {
            filterTask = task.filter(task=> task.isDone === false
            )
        }
        if(filter === 'COMPLETED') {
            filterTask = task.filter(task => task.isDone === true)
        }


  return (
    <div className="App">
        <Todolist title='What to do'
                  tasks={filterTask}
                  removeTask={removeTask}
                  changeFilter={changeFilter}
                  addTask={addTask}
                  changeInputStatus={changeInputStatus}
        />
    </div>
  );
}

export default App;
