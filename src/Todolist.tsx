import React, {ChangeEvent, useState} from "react";
import {FilterValuesType} from "./App";
import {text} from "stream/consumers";

type TasksType = {
    id:string,
    title:string,
    isDone:boolean
}

type PropsType = {
    title: string
    tasks: Array<TasksType>
    removeTask: Function
    changeFilter: (value: FilterValuesType) => void
    addTask: Function
    changeInputStatus: Function
}

export const Todolist = (props: PropsType) => {
    const [text, setText] = useState('')

    return (
        <div>
            <h3>{props.title}</h3>
            <div>
                <input
                       placeholder='Write new Task'
                       value={text}
                       onChange={(event) =>
                           setText(event.target.value)}
                />
                <button onClick={() => {props.addTask(text)}}>+</button>
            </div>
            {props.tasks.map((task,)=>{
                const handler = (e: ChangeEvent<HTMLInputElement>)=> {
                    console.log(task.id, e.currentTarget.checked)
                    props.changeInputStatus(task.id, e.currentTarget.checked)
                }
                return <ul>
                    <li>
                        <input type="checkbox"
                               checked={task.isDone}
                               key={task.id}
                               onChange={handler}

                    />
                        <span>{task.title}</span>
                        <button onClick={()=> {props.removeTask(task.id)}}>X</button>
                    </li>
                </ul>
            })}
            <div>
                <button onClick={()=> { props.changeFilter('ALL')}}>ALL</button>
                <button onClick={()=> { props.changeFilter('ACTIVE')}}>ACTIVE</button>
                <button onClick={()=> { props.changeFilter('COMPLETED')}}>COMPLETED</button>
            </div>
        </div>
    )
}